#!/bin/bash
#updating ns settings whenever we @reboot
DOMAIN='peanutbutter.ninja'
SERVER='bratwurst.peanutbutter.ninja'
ZONE='peanutbutter.ninja'
KEY_NAME='john.peanutbutter.ninja.'
KEY_VAL='z/1rbPNsS7o+aEyU5H6fowS9l7HhSkYIUmKQBuhkhhZusQ9YATKLUBsX+gomVv5EA2AFCqHFkUD7LIy8YlCkig=='
TTL=3600
HOSTNAME=${HOSTNAME}
file=$(mktemp)

echo "server $SERVER" >> $file
echo "zone $ZONE" >>$file
echo "key $KEY_NAME $KEY_VAL" >>$file
echo "update delete $HOSTNAME.$DOMAIN. AAAA">>$file

for interface in `/sbin/ip -6 addr | grep inet6 | awk -F '[ \t]+|/' '{print $3}' | grep -v ^::1 | grep -v ^fe80`
do 
	echo "update add $HOSTNAME.$DOMAIN. $TTL AAAA $interface" >>$file
done

echo "send" >>$file

if [ $( stat $file |grep Size|cut -d: -f2|awk {'print $1'} ) -gt 512 ]
then
	echo "target should support rfc2671"
fi

nsupdate $file
